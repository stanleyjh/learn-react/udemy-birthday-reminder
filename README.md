# [Birthday Reminders](https://stanleyjh.gitlab.io/learn-react/udemy-birthday-reminder/)

This application shows a reminder of who has a birthday today. The data is referenced locally from a file called data.js. It uses a State Hook to store (people - array of objects) and render the data (setPeople - function). The `people` array is passed as a prop to the List component. The List component returns each `person` object in the `people` array using the map method.

## References

[John Smilga's Udemy React Course](https://www.udemy.com/course/react-tutorial-and-projects-course/?referralCode=FEE6A921AF07E2563CEF)

[Birthday List Design](https://uidesigndaily.com/posts/sketch-birthdays-list-card-widget-day-1042)
